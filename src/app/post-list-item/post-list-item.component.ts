import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {
  like = 0;
  dislike = 0;

  date = new Date();

  @Input() postTitle: string;
  @Input() postContent: string;

  // count Likes
  clickCount() {
    this.like++;
    this.dislike === 0 ? this.dislike = 0 : this.dislike--;
  }

  // count Dislikes
  clickCountDis() {
    this.dislike++;
    this.like === 0 ? this.like = 0 : this.like--;
  }

  constructor() { }

  ngOnInit() {
  }

}
