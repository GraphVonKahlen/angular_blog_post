import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {

  posts = [
    {
      title: 'Post Title',
      content:  'One morning, when Gregor Samsa woke from troubled dreams, ' +
          'he found himself transformed in his bed into a horrible vermin. He lay on his'
    },
    {
      title: 'Post Title',
      content: 'The quick, brown fox jumps over a lazy dog. ' +
          'DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz'
    },
    {
      title: 'Post Title',
      content: 'Far far away, behind the word mountains, ' +
          'far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in'
    },
  ];
  constructor() { }

  ngOnInit() {
  }

}
